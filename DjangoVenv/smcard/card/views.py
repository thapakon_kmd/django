from unicodedata import name
from django.shortcuts import render
from matplotlib.style import context
from card.models import Card
from .forms import Card, CardForm
import qrcode
import qrcode.image.svg
from io import BytesIO


# Create your views here.
def Input(request):  
    context = {}
    form = CardForm(request.POST)
    # print(form)
    # print("HELLO 1 ")

    #print(form.is_valid())
    #print(form.errors)
    if request.method == "POST":
        form = CardForm(request.POST or None ,request.FILES or None)
        print("Hello 1")
        context = {}
        if form.is_valid():
            print("Hello 2")
            form.save()
            print("Hello 3")

            factory = qrcode.image.svg.SvgImage
            img = qrcode.make(request.POST.get("qr_text","http://127.0.0.1/id/?sm="+request.POST.get("name")), image_factory=factory, box_size=20)
            stream = BytesIO()
            img.save(stream)
            context["name"] = request.POST.get("name")
            context["svg"] = stream.getvalue().decode()
        else:
            context["form_error"] = form.errors
        
        return render(request, "qrgen.html", context)
        
    else:
        context['form'] = form
        # print(form.is_valid())
        # if form.is_valid():
        #     # save the form data to model
        #     value = form.cleaned_data("elder_surname")
        #     print(value)
        #     print("HELLO 2 ")
        #     form.save()
        #     print("HELLO 3 ")
        # else:
        #     print("Form is not valid")
        #     form = WedForm()
    
        context['form'] = form
        return render(request, "data.html", context)  

def index(request):
    
   code = request.GET['sm']
   query = Card.objects.get(name = code)

   context = {
        "sm" : query,
        "profile_img" : query.profile_img.name[10:],
   }
   return render(request, "index.html", context)

def MediaPage(request):  
    code = request.GET['sm']
    query = Card.objects.get(name = code)

    context = {
        "sm" : query,
    }
    return render(request, "Social_media.html", context)

def gamePage(request):
    code = request.GET['sm']
    query = Card.objects.get(name = code)

    context = {
        "sm" : query,
    }
    return render(request, "game.html", context)

def hobbyPage(request):
    code = request.GET['sm']
    query = Card.objects.get(name = code)

    context = {
        "sm" : query,
    }
    return render(request, "hobby.html", context)