# Generated by Django 3.2.9 on 2022-02-28 07:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('card', '0006_alter_card_profile_img'),
    ]

    operations = [
        migrations.AddField(
            model_name='card',
            name='game1',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='เกมส์ 1'),
        ),
        migrations.AddField(
            model_name='card',
            name='game2',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='เกมส์ 2'),
        ),
        migrations.AddField(
            model_name='card',
            name='game3',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='เกมส์ 3'),
        ),
        migrations.AddField(
            model_name='card',
            name='game4',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='เกมส์ 4'),
        ),
    ]
