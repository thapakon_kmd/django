# Generated by Django 3.2.9 on 2022-02-25 06:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('card', '0004_alter_card_profile_img'),
    ]

    operations = [
        migrations.AlterField(
            model_name='card',
            name='profile_img',
            field=models.ImageField(null=True, upload_to='./all_image', verbose_name='รูปโปรไฟล์'),
        ),
    ]
