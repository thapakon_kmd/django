from django.db import models

# Create your models here.
class Card(models.Model):
    name = models.CharField("ชื่อ",max_length=50, blank=True, null=True)
    surname = models.CharField("นามสกุล",max_length=50, blank=True, null=True)
    career = models.CharField("อาชีพ",max_length=50, blank=True, null=True)
    city = models.CharField("เมืองที่อยู่",max_length=50, blank=True, null=True)
    bio = models.CharField("Bio",max_length=50, blank=True, null=True)
    phone = models.CharField("เบอร์โทร",max_length=50, blank=True, null=True)


    fb = models.CharField("Facebook",max_length=50, blank=True, null=True)
    ig = models.CharField("Instagram",max_length=50, blank=True, null=True)
    tiktok = models.CharField("Tik Tok",max_length=50, blank=True, null=True)
    line = models.CharField("Line",max_length=50, blank=True, null=True)

    game1 = models.CharField("เกมส์ 1",max_length=50, blank=True, null=True)
    game2 = models.CharField("เกมส์ 2",max_length=50, blank=True, null=True)
    game3 = models.CharField("เกมส์ 3",max_length=50, blank=True, null=True)
    game4 = models.CharField("เกมส์ 4",max_length=50, blank=True, null=True)

    song = models.CharField("เพลงที่ชอบ",max_length=50, blank=True, null=True)
    actor = models.CharField("ดาราที่ชอบ",max_length=50, blank=True, null=True)
    movie = models.CharField("ภาพยนตร์ที่ชอบ",max_length=50, blank=True, null=True)
    profile_img = models.ImageField("รูปโปรไฟล์", upload_to="./all_image/", null=True)

    def __str__(self):
        return self.name