from dataclasses import field
from django import forms
from django.forms import widgets
from .models import Card

class CardForm(forms.ModelForm):
    class Meta:
        model = Card
        fields = "__all__"
        
        widgets = {
            'id': forms.TextInput(attrs={'class' : 'form-control', 'placeHolder' : 'ID'}),
            'name': forms.TextInput(attrs={'class' : 'form-control', 'placeHolder' : 'name'}),
            'surname': forms.TextInput(attrs={'class' : 'form-control', 'placeHolder' : 'surname'}),
        }

