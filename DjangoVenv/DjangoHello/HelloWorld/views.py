from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def hello(request):
    return HttpResponse("Hello World, Welcome to Thailand")

def greeting(request):
    username = request.GET['name']
    age = request.GET['age']
    response = "Hello World Khun: " + username + " <br>"
    response = response + "You are " + age + " years old <br> "

    if int(age) < 18:
        response = response + "You are still under 18."
    else:
        response = response + "You are over 18."

    return HttpResponse(response)