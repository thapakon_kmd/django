from distutils.command.upload import upload
from django.db import models
from django.contrib.auth.models import UserManager

# Create your models here.
GENDER_choice = (
    ('female', 'หญิง'),
    ('male', 'ชาย'),
    ('other', 'อื่น ๆ')
)
Blood_Group = {
    ('ab', 'AB'),
    ('a','A'),
    ('b','B'),
    ('o','O'),
}

class Wb(models.Model):

    elder_name = models.CharField("ชื่อ",max_length=50, blank=True, null=True)
    elder_surname = models.CharField("นามสกุล",max_length=50, blank=True, null=True)
    elder_id = models.CharField("เลข ID", max_length=7, primary_key=True)
    elder_nickname = models.CharField("ชื่อเล่น",max_length=50, blank=True, null=True)
    elder_gender = models.CharField("เพศ",max_length=50, choices= GENDER_choice, blank=True, null=True)
    elder_birth = models.DateField("วันเกิด",blank=True, null=True)
    elder_age = models.SmallIntegerField("อายุ",blank=True, null=True)
    elder_weight = models.CharField("น้ำหนัก",max_length=50, blank=True, null=True)
    elder_height = models.CharField("ส่วนสูง",max_length=50, blank=True, null=True)
    elder_blood = models.CharField("กรุ๊ปเลือด",max_length=7,choices= Blood_Group, blank=True, null=True)
    elder_disease = models.CharField("โรคประจำตัว",max_length=50, blank=True, null=True)
    elder_food = models.CharField("อาหารที่แพ้",max_length=50, blank=True, null=True)
    elder_medicine = models.CharField("ยาที่แพ้",max_length=50, blank=True, null=True)
    elder_useMedicine = models.CharField("ยาที่ใช้ประจำ",max_length=50, blank=True, null=True)
    elder_address = models.TextField("ที่อยู่",blank=True, null=True)
    elder_cure = models.TextField("การรักษา",blank=True, null=True)
    elder_help = models.TextField("การให้ความช่วยเหลือ",blank=True, null=True)
    elder_phone = models.CharField("เบอร์โทร",max_length=50, blank=True, null=True)
    elder_fb = models.CharField("Facebook",max_length=50, blank=True, null=True)
    elder_line = models.CharField("Line",max_length=50, blank=True, null=True)
    elder_song = models.CharField("เพลงที่ชอบ",max_length=50, blank=True, null=True)
    elder_actor = models.CharField("ดาราที่ชอบ",max_length=50, blank=True, null=True)
    elder_movie = models.CharField("ภาพยนตร์ที่ชอบ",max_length=50, blank=True, null=True)
    profile_img = models.ImageField("รูปโปรไฟล์", upload_to="./wed/static/assets/img", null=True)

    objects = UserManager()


    class Meta:
        db_table = "wb"

    def __str__(self):
        return self.elder_id

