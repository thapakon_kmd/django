from django.shortcuts import render
from wed.models import Wb
from .forms import Wb, WedForm
import qrcode
import qrcode.image.svg
from io import BytesIO

# Create your views here.
def Input(request):  
    context = {}
    form = WedForm(request.POST)
    # print(form)
    # print("HELLO 1 ")

    #print(form.is_valid())
    #print(form.errors)
    if request.method == "POST":
        form = WedForm(request.POST or None ,request.FILES or None)
        print("Hello 1")
        context = {}
        if form.is_valid():
            print("Hello 2")
            form.save()
            print("Hello 3")

            factory = qrcode.image.svg.SvgImage
            img = qrcode.make(request.POST.get("qr_text","http://127.0.0.1/id/?elder="+request.POST.get("elder_id")), image_factory=factory, box_size=20)
            stream = BytesIO()
            img.save(stream)
            context["elder_id"] = request.POST.get("elder_id")
            context["svg"] = stream.getvalue().decode()
        else:
            context["form_error"] = form.errors
        
        return render(request, "qrgen.html", context)
        
    else:
        context['form'] = form
        # print(form.is_valid())
        # if form.is_valid():
        #     # save the form data to model
        #     value = form.cleaned_data("elder_surname")
        #     print(value)
        #     print("HELLO 2 ")
        #     form.save()
        #     print("HELLO 3 ")
        # else:
        #     print("Form is not valid")
        #     form = WedForm()
    
        context['form'] = form
        return render(request, "data.html", context)  

def index(request):

   elder_code = request.GET['elder']
   elder_query = Wb.objects.get(elder_id = elder_code)

   context = {
        "elder" : elder_query,
        "profile_img" : elder_query.profile_img.name[10:]
   }
   return render(request, "index.html", context)





