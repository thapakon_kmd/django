from django.apps import AppConfig


class DjangoforumConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'djangoforum'
