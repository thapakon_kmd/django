from django.db import models

# Create your models here.
class Author(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    def __str__(self): 
        return self.first_name + " " + self.last_name

class Post(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    post_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField()

    def __str__(self):
        return self.author.first_name + ":" + self.post_text + " post on :" + str(self.pub_date)

