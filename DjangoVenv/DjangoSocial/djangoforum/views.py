from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers

# Create your views here.

from djangoforum.models import Post, Author

def list_authors(request):
    authors = Author.objects.all()
    return HttpResponse(serializers.serialize('json',authors))

def list_posts(request):
    posts = Post.objects.all()
    return HttpResponse(serializers.serialize('json', posts))

def index(request):
    post_list = Post.objects.all()
    context = {
        'post_list' : post_list,
    }
    return render(request, 'index.html', context)

