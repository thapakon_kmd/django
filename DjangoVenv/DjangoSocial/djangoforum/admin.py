from django.contrib import admin
from djangoforum.models import Author, Post

# Register your models here.

admin.site.register(Author)
admin.site.register(Post)