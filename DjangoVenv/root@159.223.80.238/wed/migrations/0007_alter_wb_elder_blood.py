# Generated by Django 3.2.9 on 2022-02-03 19:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wed', '0006_auto_20220203_1803'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wb',
            name='elder_blood',
            field=models.CharField(blank=True, choices=[('a', 'A'), ('ab', 'AB'), ('b', 'B'), ('o', 'O')], max_length=7, null=True, verbose_name='กรุ๊ปเลือด'),
        ),
    ]
