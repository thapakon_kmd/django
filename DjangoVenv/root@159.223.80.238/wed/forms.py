from dataclasses import field
from django import forms
from django.forms import widgets
from .models import Wb

class WedForm(forms.ModelForm):
    class Meta:
        model = Wb
        fields = "__all__"
        
        widgets = {
            'elder_birth': widgets.DateInput(attrs={'type': 'date'}),
            'elder_ID': forms.TextInput(attrs={'class' : 'form-control', 'placeHolder' : 'ID'}),
            'elder_name': forms.TextInput(attrs={'class' : 'form-control', 'placeHolder' : 'name'}),
            'elder_surname': forms.TextInput(attrs={'class' : 'form-control', 'placeHolder' : 'surname'}),
            'elder_nickname': forms.TextInput(attrs={'class' : 'form-control', 'placeHolder' : 'nickname'}),
            'elder_gender': forms.Select(attrs={'class' : 'form-control'}),
            'elder_age': forms.TextInput(attrs={'class' : 'form-control', 'placeHolder' : 'age'}),
            'elder_weight': forms.TextInput(attrs={'class' : 'form-control', 'placeHolder' : 'weight'}),
            'elder_height': forms.TextInput(attrs={'class' : 'form-control', 'placeHolder' : 'height'}),
            'elder_blood' : forms.Select(attrs={'class' : 'form-control'})
        }

