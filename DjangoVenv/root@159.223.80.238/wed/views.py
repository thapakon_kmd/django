from django.shortcuts import render
from wed.models import Wb
from .forms import Wb, WedForm
import qrcode
import qrcode.image.svg
from io import BytesIO

# Create your views here.

def Save(request):
    context = {}   
    if request.method == "POST":
        factory = qrcode.image.svg.SvgImage
        img = qrcode.make(request.POST.get("qr_text","www.google.com"), image_factory=factory, box_size=20)
        stream = BytesIO()
        img.save(stream)
        context["svg"] = stream.getvalue().decode()

    return render(request, "home.html", context=context)

  
def Input(request):  
    context = {}
    form = WedForm(request.POST)
    # print(form)
    # print("HELLO 1 ")

    print(form.is_valid())
    if request.method == "POST":
        form = WedForm(request.POST or None ,request.FILES or None)
        print("Hello 1")
        if form.is_valid():
            print("Hello 2")
            form.save()
            print("Hello 3")
    else:
        form = WedForm()
    # print(form.is_valid())
    # if form.is_valid():
    #     # save the form data to model
    #     value = form.cleaned_data("elder_surname")
    #     print(value)
    #     print("HELLO 2 ")
    #     form.save()
    #     print("HELLO 3 ")
    # else:
    #     print("Form is not valid")
    #     form = WedForm()
    
    context['form'] = form
    return render(request, "data.html", context)  

def index(request):

   elder_code = request.GET['elder']
   elder_query = Wb.objects.get(elder_id = elder_code)

   context = {
        "elder" : elder_query,
        "profile_img" : elder_query.profile_img.name[10:]
   }
   return render(request, "index.html", context)





